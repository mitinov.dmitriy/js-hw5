window.addEventListener("load", function () {

    function createNewUser() {
        let userName = prompt("Enter user's name");
        while (!isValidStringInput(userName)) {
            userName = prompt("Please re-enter user's name");
        }
        let userSecondName = prompt("Enter user's second name");
        while (!isValidStringInput(userSecondName)) {
            userSecondName = prompt("Please re-enter user's second name");
        }
        let userBirthday = prompt("Please enter user's birth date. Format - dd.mm.yyyy")
        while (!isValidDateFormat(userBirthday)) {
            userBirthday = prompt("Invalid Date! Please re-enter user's birth date. Format - dd.mm.yyyy")
        }
        let newUser = {
            _firstName: userName,
            _lastName: userSecondName,
            _birthday: new Date(userBirthday.split(".").reverse().join("/")),
            get firstName() {
                return this._firstName
            },
            get lastName() {
                return this._lastName
            },
            get birthday() {
                return this._birthday;
            },
            setFirstName: function (newName) {
                this._firstName = newName;
            },
            setLastName: function (newLastName) {
                this._lastName = newLastName;
            },
            getLogin: function () {
                return `${this.firstName[0].toLowerCase()}${this.lastName.toLowerCase()}`
            },
            getAge: function () {
                const currentDate = new Date(Date.now());
                return Math.trunc((currentDate - this.birthday) / 31536000000);
            },
            getPassword: function () {
                return `${this.firstName[0].toUpperCase()}${this.lastName.toLowerCase()}${this.birthday.getFullYear()}`
            }

        };
        return newUser;
    }

    let admin = createNewUser();
    console.log(admin.getLogin());
    console.log(admin.getAge());
    console.log(admin.getPassword());


    function isValidStringInput(string) {
        return !(!string || string.trim() === "" || !isNaN(+string));
    }

    function isValidDateFormat(dateString) {
        const regExp = new RegExp("^(0[1-9]|[12][0-9]|3[01])[.](0[1-9]|1[012])[.](19|20)[0-9]{2}$");
        return regExp.test(dateString);
    }
});
